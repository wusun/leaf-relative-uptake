# Data and code for the modeling and analysis of leaf COS : CO<sub>2</sub> relative uptake, published in _New Phytologist_

[![doi](https://zenodo.org/badge/doi/10.5281/zenodo.6423911.svg)](https://doi.org/10.5281/zenodo.6423911)

Wu Sun (<wsun@carnegiescience.edu>)

Department of Global Ecology, Carnegie Institution for Science

Last updated on 2022-08-12

This repository presents data and code used to produce results in the following
work:

Sun, W., Berry, J. A., Yakir, D., & Seibt, U. (2022). Leaf relative uptake of
carbonyl sulfide to CO<sub>2</sub> seen through the lens of stomatal
conductance–photosynthesis coupling. _New Phytologist_, *235*(5), 1729–1742.
<https://doi.org/10.1111/nph.18178>

<details>
  <summary>BibTeX citation</summary>

```bibtex
@article{sun_et_al_2022_np,
  author  = {Wu Sun and Joseph A. Berry and Dan Yakir and Ulli Seibt},
  title   = {Leaf carbonyl sulfide to {CO\textsubscript{2}} relative uptake seen through the lens of stomatal conductance--photosynthesis coupling},
  journal = {New Phytologist},
  volume  = {235},
  number  = {5},
  pages   = {1729--1742},
  doi     = {10.1111/nph.18178},
  url     = {https://nph.onlinelibrary.wiley.com/doi/abs/10.1111/nph.18178},
  eprint  = {https://nph.onlinelibrary.wiley.com/doi/pdf/10.1111/nph.18178},
  year    = {2022}
}
```
</details>

## Data

The following data sets are provided here for reproducing the results in the
present study only. For reuse, please access the original data sets and/or
contact the original data providers.

### San Joaquin Freshwater Marsh leaf flux data

* File: [`data/sjm_ch_filtered.csv`](./data/sjm_ch_filtered.csv)
* Original publication: Sun, W., Maseyk, K., Lett, C., & Seibt, U. (2018).
  Stomatal control of leaf fluxes of carbonyl sulfide and CO<sub>2</sub> in a
  _Typha_ freshwater marsh. _Biogeosciences_, _15_(11), 3277–3291.
  <https://doi.org/10.5194/bg-15-3277-2018>

Leaf flux data from the San Joaquin Freshwater Marsh are available at
<https://doi.org/10.15146/R37T00> under a
[CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)
license.

### Stimler et al. leaf flux data

* File: [`data/stimler-light-and-ci-subset.csv`](./data/stimler-light-and-ci-subset.csv)
* Original publications
  + Stimler, K., Montzka, S. A., Berry, J. A., Rudich, Y., & Yakir, D. (2010).
    Relationships between carbonyl sulfide (COS) and CO<sub>2</sub> during leaf
    gas exchange. _New Phytologist_, _186_(4), 869–878.
    <https://doi.org/10.1111/j.1469-8137.2010.03218.x>
  + Stimler, K., Berry, J. A., Montzka, S. A., & Yakir, D. (2011). Association
    between carbonyl sulfide uptake and <sup>18</sup>Δ during gas exchange in
    C<sub>3</sub> and C<sub>4</sub> leaves. _Plant Physiology_, _157_(1),
    509–517. <https://doi.org/10.1104/pp.111.176578>

Note that only a subset of this data set is used in the present study. Requests
for the full data set shall be addressed to the original data provider.

## Code

Python code and Jupyter notebooks to reproduce the figures are in the `src`
folder. You may need the following prerequisites to run the code.

* Python >= 3.9 (Python 2.x not supported)
* Jupyter Notebook
* numpy
* scipy
* pandas
* statsmodels
* matplotlib
* seaborn
* R >= 3.6
* RPy2

## License

This repository is licensed under the [MIT License](./LICENSE).

`SPDX-License-Identifier: MIT`

You do not need written permission from the authors to use this repository.

Please contact Wu Sun (wsun@carnegiescience.edu) if you have any questions.
