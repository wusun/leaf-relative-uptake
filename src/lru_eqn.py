# Leaf carbonyl sulfide relative uptake
#
# Copyright (c) 2019--2022 Wu Sun <wsun@carnegiescience.edu>
# Department of Global Ecology
# Carnegie Institution for Science
"""Leaf COS : CO2 relative uptake equation."""
from typing import Union

import numpy as np

RATIO_GS_H2O_COS: float = 1.94  # Stimler et al. (2010) New Phytologist
RATIO_GS_H2O_CO2: float = 1.6

RATIO_GB_H2O_COS: float = RATIO_GS_H2O_COS ** (2.0 / 3.0)
RATIO_GB_H2O_CO2: float = RATIO_GS_H2O_CO2 ** (2.0 / 3.0)


def rel_uptake(
    m: float,
    g_is: float,
    assim: Union[float, np.ndarray],
    rh: Union[float, np.ndarray],
    co2: Union[float, np.ndarray],
) -> Union[float, np.ndarray]:
    """
    Calculate leaf COS : CO2 relative uptake ratio.

    Parameters
    ----------
    m : float
        Ball--Berry slope parameter.
    g_is : float
        Internal conductance to COS [mol m^-2 s^-1].
    assim : array_like
        CO2 assimilation rate [µmol m^-2 s^-1].
    rh : array_like
        Relative humidity at the leaf surface [0--1].
    co2 : array_like
        CO2 mixing ratio at the leaf surface [µmol mol^-1].

    Returns
    -------
    array_like
        Leaf COS : CO2 relative uptake ratio (dimensionless).

    """
    return 1 / (RATIO_GS_H2O_COS / (m * rh) + assim / (g_is * co2))


def electron_transport(
    j_max: float, theta: float, f_psii: float, ppfd: Union[float, np.ndarray]
) -> Union[float, np.ndarray]:
    """
    Calculate the linear electron transport rate.

    Parameters
    ----------
    j_max : float
        Maximum electron transport rate [µmol m^-2 s^-1].
    theta : float
        A dimensionless curvature factor [0--1].
    f_psii : float
        Fraction of absorbed radiation at PSII, dimensionless.
    ppfd : array_like
        Photosynthetic photon flux density [µmol m^-2 s^-1].

    Returns
    -------
    array_like
        Linear electron transport rate [µmol m^-2 s^-1].

    """
    i_psii = ppfd * f_psii
    x = i_psii + j_max  # an intermediate variable
    if abs(theta) > 1e-8:
        return (x - np.sqrt((x * x) - (4.0 * theta * i_psii * j_max))) / (
            2.0 * theta
        )
    else:
        return i_psii * j_max / x


def assim_coupled(
    m: float,
    v_cmax: float,
    k_co: float,
    co2_comp: float,
    resp: float,
    j_max: float,
    theta: float,
    f_psii: float,
    ppfd: Union[float, np.ndarray],
    rh: Union[float, np.ndarray],
    co2: Union[float, np.ndarray],
) -> Union[float, np.ndarray]:
    """
    Calculate the coupled assimilation rate.

    Parameters
    ----------
    m : float
        Ball--Berry slope parameter.
    v_cmax : float
        Maximum carboxylation rate of RuBisCO [µmol m^-2 s^-1].
    k_co : float
        Combined Michaelis constant for carboxylation and oxygenation of
        RuBisCO [µmol mol^-1].
    co2_comp : float
        CO2 compensation point [µmol mol^-1].
    resp : float
        Day respiration rate [µmol m^-2 s^-1].
    j_max : float
        Maximum electron transport rate [µmol m^-2 s^-1].
    theta : float
        A dimensionless curvature factor [0--1].
    f_psii : float
        Fraction of absorbed radiation at PSII, dimensionless.
    ppfd : array_like
        Photosynthetic photon flux density [µmol m^-2 s^-1].
    rh : array_like
        Relative humidity at the leaf surface [0 to 1].
    co2 : array_like
        CO2 mixing ratio at the leaf surface [µmol mol^-1].

    Returns
    -------
    array_like
        CO2 assimilation rate [µmol m^-2 s^-1].

    """
    j = electron_transport(j_max, theta, f_psii, ppfd)
    co2_i = (1 - RATIO_GS_H2O_CO2 / (m * rh)) * co2
    assim_j = (co2_i - co2_comp) * j / (4 * co2_i + 8 * co2_comp)
    assim_c = (co2_i - co2_comp) * v_cmax / (co2_i + k_co)
    assim = np.fmin(assim_j, assim_c) - resp
    return assim


def rel_uptake_coupled(
    m: float,
    g_is: float,
    v_cmax: float,
    k_co: float,
    co2_comp: float,
    resp: float,
    j_max: float,
    theta: float,
    f_psii: float,
    ppfd: Union[float, np.ndarray],
    rh: Union[float, np.ndarray],
    co2: Union[float, np.ndarray],
) -> Union[float, np.ndarray]:
    """
    Calculate leaf COS : CO2 relative uptake ratio from coupled assimilation.

    Parameters
    ----------
    m : float
        Ball--Berry slope parameter.
    g_is : float
        Internal conductance to COS [mol m^-2 s^-1].
    v_cmax : float
        Maximum carboxylation rate of RuBisCO [µmol m^-2 s^-1].
    k_co : float
        Combined Michaelis constant for carboxylation and oxygenation of
        RuBisCO [µmol mol^-1].
    co2_comp : float
        CO2 compensation point [µmol mol^-1].
    resp : float
        Day respiration rate [µmol m^-2 s^-1].
    j_max : float
        Maximum electron transport rate [µmol m^-2 s^-1].
    theta : float
        A dimensionless curvature factor [0--1].
    f_psii : float
        Fraction of absorbed radiation at PSII, dimensionless.
    ppfd : array_like
        Photosynthetic photon flux density [µmol m^-2 s^-1].
    rh : array_like
        Relative humidity at the leaf surface [0 to 1].
    co2 : array_like
        CO2 mixing ratio at the leaf surface [µmol mol^-1].

    Returns
    -------
    array_like
        Leaf COS : CO2 relative uptake ratio (dimensionless).

    """
    assim = assim_coupled(
        m, v_cmax, k_co, co2_comp, resp, j_max, theta, f_psii, ppfd, rh, co2,
    )
    return rel_uptake(m, g_is, assim, rh, co2)


def lossfun_rel_uptake(
    m: float,
    g_is: float,
    assim: Union[float, np.ndarray],
    rh: Union[float, np.ndarray],
    co2: Union[float, np.ndarray],
    obs: np.ndarray,
) -> float:
    """L2 loss function for leaf relative uptake ratio."""
    residuals = rel_uptake(m, g_is, assim, rh, co2) - obs
    return np.mean(residuals * residuals) * 0.5


def lossfun_assim_coupled(
    m: float,
    v_cmax: float,
    k_co: float,
    co2_comp: float,
    resp: float,
    j_max: float,
    theta: float,
    f_psii: float,
    ppfd: Union[float, np.ndarray],
    rh: Union[float, np.ndarray],
    co2: Union[float, np.ndarray],
    obs: np.ndarray,
) -> float:
    """Loss function for the coupled assimilation rate."""
    residuals = (
        assim_coupled(
            m,
            v_cmax,
            k_co,
            co2_comp,
            resp,
            j_max,
            theta,
            f_psii,
            ppfd,
            rh,
            co2,
        )
        - obs
    )
    return np.mean(residuals * residuals) * 0.5


def lossfun_rel_uptake_coupled(
    m: float,
    g_is: float,
    v_cmax: float,
    k_co: float,
    co2_comp: float,
    resp: float,
    j_max: float,
    theta: float,
    f_psii: float,
    ppfd: Union[float, np.ndarray],
    rh: Union[float, np.ndarray],
    co2: Union[float, np.ndarray],
    obs: np.ndarray,
) -> float:
    """Loss function for coupled equations of leaf relative uptake ratio."""
    residuals = (
        rel_uptake_coupled(
            m,
            g_is,
            v_cmax,
            k_co,
            co2_comp,
            resp,
            j_max,
            theta,
            f_psii,
            ppfd,
            rh,
            co2,
        )
        - obs
    )
    return np.mean(residuals * residuals) * 0.5
